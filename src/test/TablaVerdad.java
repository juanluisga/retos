package test;

import static java.lang.Math.pow;

public class TablaVerdad {

    public static void main(String[] args){

        final int elementos = 4;
        for (int i = 0; i < pow(2, elementos); i += 2){
            // quedamos solo los pares porque al primer pulsador siempre va un único ratón.
            System.out.println(String.format("%" + elementos + "s", Integer.toBinaryString(i)).replace(' ', '0'));
        }
    }
}
